$(function () {
    
    $('#date').datetimepicker({
      icons: {
        time: 'fa fa-clock-o',
        date: 'fa fa-calendar',
        up: 'fa fa-arrow-up',
        down: 'fa fa-arrow-down',
        previous: 'fa fa-arrow-left',
        next: 'fa fa-arrow-right',
      },
      format:'DD/MM/YYYY',
    });

    $(".controller-alumno").click(function() {

        let matricula = $('#matricula').val();
        let nombre = $('#nombre').val();
        let fecha = DateFormat($('#date').val());
        let id = $('#id').val();
        
        let controller  = dominio_host+"admin/addAlumno";

        if(id!=""){
            controller  = dominio_host+"admin/editAlumno";
        }

        $.ajax({
            url: controller,
            type: 'POST',
            dataType: 'json',
            data: {
                nombre: nombre,
                matricula: matricula,
                fecha: fecha,
                id: id,
            },
            cache: false,
            success: function(response) {

                if (response.status=='success') {
                    
                    $.notify(response.message, "success");
                    setTimeout(() => { $('#exampleModal').modal('hide'); }, 2000);
                    document.getElementById("alumnosForm").reset();
                    var table = $('#alumnosTables').DataTable();
                    table.ajax.reload();
                
                } else {
                    $.notify(response.message, "error");
                }
            },
            error: function(jqXHR, exception) {
                $.notify(jqXHR.responseText, "error");
            }
        });
    });

    $(".controller-materia").click(function() {

        let clave_materia = $('#clave_materia').val();;
        let nombre = $('#nombre').val();
        let id = $('#id').val();
        
        let controller  = dominio_host+"admin/addMateria";

        if(id!=""){
            controller  = dominio_host+"admin/editMateria";
        }

        $.ajax({
            url: controller,
            type: 'POST',
            dataType: 'json',
            data: {
                clave_materia: clave_materia,
                nombre: nombre,
                id: id,
            },
            cache: false,
            success: function(response) {

                if (response.status=='success') {
                    
                    $.notify(response.message, "success");
                    setTimeout(() => { $('#exampleModal').modal('hide'); }, 2000);
                    document.getElementById("materiaForm").reset();
                    var table = $('#materiasTables').DataTable();
                    table.ajax.reload();
                
                } else {
                    $.notify(response.message, "error");
                }
            },
            error: function(jqXHR, exception) {
                $.notify(jqXHR.responseText, "error");
            }
        });
    });

    $(".controller-asignacion").click(function() {

        let id_materia = $('#id_materia').val();
        let id_alumnos = $('#id_alumno').val();
        
        let controller  = dominio_host+"admin/addAsignacion";

        $.ajax({
            url: controller,
            type: 'POST',
            dataType: 'json',
            data: {
                id_materia: id_materia,
                id_alumnos: id_alumnos,
            },
            cache: false,
            success: function(response) {

                if (response.status=='success') {
                    
                    $.notify(response.message, "success");
                    document.getElementById("asignacionForm").reset();
                    setTimeout(() => { $('#exampleModal').modal('hide'); }, 2000);
                    var table = $('#asignacionesTables').DataTable();
                    table.ajax.reload();
                
                } else {
                    $.notify(response.message, "error");
                }
            },
            error: function(jqXHR, exception) {
                $.notify(jqXHR.responseText, "error");
            }
        });
    });

    $("#alumnosTables").DataTable({
        ajax: dominio_host + "admin/getAlumnoTable",
        destroy: true,
        columns: [
            {
                data: "id",
            },
            {
                data: "matricula",
            },
            {
                data: "nombre",
            },
            {
                data: null,
                render: function ( data, type, row ) {
                    if(row.fecha_registro){
                        return convertDateFormat(row.fecha_registro);
                    }
                },
                defaultContent: "N/A"
            },
            {
                data: null,
                render: function (data, type, row) {
                    var botones ='<a href="javascript:edit('+ row.id +');" class="btn btn-info btn-xs">';
                    botones+='<i class="fa fa-edit"></i></a>';
                    botones +='<a href="#" style="display:"none;">&nbsp;&nbsp;</a>';
                    botones +='<a href="javascript:remove('+ row.id +');" class="btn btn-danger btn-xs">';
                    botones +='<i class="fa fa-trash"></i></a>';
    
                    return botones;
                },
                defaultContent: "N/A",
            },
        ],
        destroy: true,
        responsive: true,
        bPaginate: true,
        autoWidth: true,
        bInfo: true,
        bSort: true,
        scrollX: false,
        bLengthChange: true,
        order: [[0, "desc"]],
    });

    $("#materiasTables").DataTable({
        ajax: dominio_host + "admin/getMateriaTable",
        destroy: true,
        columns: [
            {
                data: "id",
            },
            {
                data: "clave_materia",
            },
            {
                data: "nombre",
            },
            {
                data: null,
                render: function (data, type, row) {
                    var botones ='<a href="javascript:edit('+ row.id +');" class="btn btn-info btn-xs">';
                    botones+='<i class="fa fa-edit"></i></a>';
                    botones +='<a href="#" style="display:"none;">&nbsp;&nbsp;</a>';
                    botones +='<a href="javascript:remove('+ row.id +');" class="btn btn-danger btn-xs">';
                    botones +='<i class="fa fa-trash"></i></a>';
    
                    return botones;
                },
                defaultContent: "N/A",
            },
        ],
        destroy: true,
        responsive: true,
        bPaginate: true,
        autoWidth: true,
        bInfo: true,
        bSort: true,
        scrollX: false,
        bLengthChange: true,
        order: [[0, "desc"]],
    });


    $("#asignacionesTables").DataTable({
        ajax: dominio_host + "admin/getAsignacionesTable",
        destroy: true,
        columns: [
            {
                data: "id",
            },
            {
                data: "matricula",
            },
            {
                data: "nombre_alumno",
            },
            {
                data: "nombre_materia",
            },
            {
                data: "clave_materia",
            },
            {
                data: null,
                render: function (data, type, row) {
                    var botones ='<a href="javascript:remove('+ row.id +');" class="btn btn-danger btn-xs">';
                    botones +='<i class="fa fa-trash"></i></a>';
    
                    return botones;
                },
                defaultContent: "N/A",
            },
        ],
        destroy: true,
        responsive: true,
        bPaginate: true,
        autoWidth: true,
        bInfo: true,
        bSort: true,
        scrollX: false,
        bLengthChange: true,
    });


    function convertDateFormat(string) {
        var info = string.split('-');
        return info[2].substring(0,2) + '/' + info[1] + '/' + info[0];
    }

    function DateFormat(string) {
        var info = string.split('/');
        return info[2].substring(0,4) + '-' + info[1] + '-' + info[0];
    }

    $("#id_materia").select2({ width: '100%' });
    $("#id_alumno").select2({ width: '100%' });
});